<?php
// This file is part of the Xpert URL download repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL downloader (Xpert) image attribution
 * @package    repository_xpert_url
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * URL downloader (Xpert) image attribution class
 * @package    repository_xpert_url
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_url_image {

    /** Default text + license icon layout */
    const LAYOUT_DEFAULT = 0;
    /**  The attribution be adding beside the license icon?  */
    const LAYOUT_ATTRIBUTION_BESIDE_LICENSE = 1;
    /**  The attribution should be adding below the license icon  */
    const LAYOUT_ATTRIBUTION_BELOW_LICENSE = 2;
    /**  The attribution text and URL should be aligned next to the license icon  */
    const LAYOUT_MIXED_ALIGN_URL = 3;
    /**  The attribution URL should be allowed to overflow to multiple lines  */
    const LAYOUT_MIXED_OVERFLOW_URL = 4;
    /**  The attribution URL should be beside the license icon  */
    const LAYOUT_URL_BESIDE_LICENSE = 5;
    /**  The attribution text and URL should be allowed to overflow to multiple lines  */
    const LAYOUT_OVERFLOW_ALL = 6;

    /** @var string The path to the selected image  */
    public $imagepath;
    /** @var array Information about the selected image */
    public $info;
    /** @var int Width of the selected image */
    public $width;
    /** @var int Height of the selected image */
    public $height;
    /** @var resource Image resource of the selected image */
    public $image;
    /** @var resource $debugimage The debug image resource identifier */
    public $debugimage;
    /** @var int $textlines The number of lines needed for year + author*/
    public $textlines = 1;
    /** @var int The number of lines needed to add the URL */
    public $urllines = 1;
    /** @var array The colour of the copyright text */
    public $textredgreenblue;
    /** @var array The background colour of the copyright text */
    public $backgroundredgreenblue;
    /** @var int The fontsize for the copyright text */
    public $fontsize = 8;
    /** @var int The number of characters in the Year + Author section of the copyright text */
    public $textchars = 0;
    /** @var int The number of characters in the URL section of the copyright text*/
    public $urlchars = 0;
    /** @var boolean Whether the URL can fit next to the license icon without overflowing */
    public $textfits = false;
    /** @var string The text to fit next to the license icon (for small images) */
    public $texttofit = '';
    /** @var boolean Whether to align both sets of text (Author & URL) */
    public $aligntext = true;
    /** @var resource Extra text image to fit text next to the license icon (for small images)  */
    public $extratextimage;
    /** @var string The URL to be used as part of the copyright text */
    public $url;
    /** @var int The fontcolour colour of the copyright text */
    public $fontcolour;
    /** @var int The font of the copyright text */
    public $font;
    /** @var string The license to be used in the copyright text */
    public $license;
    /** @var resource Resource of the Creative Commons license icon */
    public $licenseicon;
    /** @var string $size The selected image size */
    public $size;
    /** @var string Year + Author text to be used in the copyright text */
    public $yearauthor;
    /** @var int The height of the copyright license icon */
    public $iconheight;
    /** @var int The width of the copyright license icon */
    public $iconwidth;
    /** @var int The height of the master image on which to paste the main image, license icon & text */
    public $newheight;
    /** @var resource Resource of the master image on which to paste the main image, license icon & text*/
    public $masterimage;
    /** @var resource Resource of the filler image */
    public $fillerimage;
    /** @var resource Resource of image which will contain the copyright text */
    public $textimage;
    /** @var int Height of the copyright text in pixels */
    public $textheight;
    /** @var int Height of the image which will contain the copyright text */
    public $textimageheight;
    /** @var int Width of the image which will contain the copyright text */
    public $textimagewidth;
    /** @var int X position of the image which will contain the copyright text */
    public $xposition;
    /** @var int The image resource of the master image (blank canvas) on which the other images will be copied onto  */
    public $yposition;
    /** @var int Whether debugging is on or off */
    public $debugging;
    /** @var int Number of extra lines */
    public $extralines = 0;
    /** @var int Total number of lines */
    public $totallines;
    /** @var int The width of the Year + Author text  */
    public $textwidth;
    /** @var int The width of the URL text */
    public $urlwidth;
    /** @var string The particular of the license icon and copyright information */
    public $layout;
    /** @var int The width of the extra text image (used sometimes to cater for overflowing lines */
    public $boxwidth;

    /**
     * Constructor for repository_xpert_url class
     *
     * @param object $img
     * @param string $author
     * @param string $url
     * @param string $license
     * @param string $size
     * @param int $year
     * @param string $colourcombo
     * @throws moodle_exception
     */
    public function __construct($img, $author, $url, $license, $size, $year, $colourcombo) {
        global $CFG;

        if ($author == '' || !$author) {
            throw new moodle_exception('noauthorspecified', 'repository_xpert_url');
        }
        $this->debugging = $this->check_debugging_status();

        if (!function_exists('imagecreatefrompng') and !function_exists('imagecreatefromjpeg')) {
            throw new moodle_exception('gdnotexist');
        }
        if (!file_exists($img) or !is_readable($img)) {
            throw new moodle_exception('invalidfile');
        }

        if (!clean_param($url, PARAM_PATH) && $url <> '' && clean_param($url, PARAM_URL) == '') {
            throw new moodle_exception('invalidurl', 'repository_xpert_url');
        }

        $this->imagepath = $img;
        unset($img);
        $this->size = $this->get_image_size($size);
        $this->image = $this->resize_image();
        $this->yearauthor = $this->get_yearauthor($year, $author, $url);
        $this->url = $this->get_url($url, $year);
        $this->font = $CFG->libdir . '/default.ttf';
        $this->license = $license;

        if ($this->yearauthor == '' && $this->url == '' && ($license == 'unknown' || $license == 'allrightsreserved')) {
            return;
        } else {
            // Set the font size for the copyright text.
            $this->set_font_size();
            // Set the background and text colours.
            $this->set_colour_combination($colourcombo);
            // Create a Creative Commons license icon.
            $this->set_licenseicon_properties();
            // Set the x & y position for placing the image containing the copyright text.
            $this->set_xy_position();
            // Store specific info for each image size (font size, lines, strings, characters).
            $this->set_text_info();
            // Create the image which will hold the copyright information.
            $this->create_text_image();
            // Where necessary, create other images which will be pasted on to the master image.
            $this->create_other_images();
            // Add the copyright text to the blank image created for this purpose ($this->textimage).
            if ($this->yearauthor <> '') {
                // Add copyright text for Year + Author.
                $this->wrap_image_text($this->yearauthor, $this->textchars->perline, 1, $this->textlines);
                if ($this->url <> '') {
                    // Add copyright text for URL.
                    $this->wrap_image_text($this->url, $this->urlchars->perline, $this->textlines + 1, $this->urllines);
                }
            } else if ($this->url <> '') {
                // Add copyright text for only the URL as no author has been specified.
                $this->wrap_image_text($this->url, $this->urlchars->perline, 1, $this->urllines);
            }
            $this->add_attribution();
        }
    }

    /**
     * Where necessary, create other images which will be pasted on to the master image
     */
    private function create_other_images() {
        // Create extra image to fit text for small images into empty space.
        if ($this->size == 'small' && $this->textfits && !$this->aligntext) {
            $this->create_extra_text_image();
        }
        // Create extra image to fill up black space below icon if needed.
        if ($this->size <> 'small' && $this->textimageheight > $this->iconheight
                || $this->layout === self::LAYOUT_MIXED_ALIGN_URL) {
            $this->create_filler_image();
        }
        // Add debug info to master image when debugging for quick visual feedback.
        if ($this->debugging) {
            $this->add_debug_info();
        }
    }

    /**
     * Set the red, green and blue values for the background and text colour
     *
     * @param string $combo
     * @throws moodle_exception
     */
    public function set_colour_combination($combo) {
        $combos = array();
        $combos['colourwhiteblack']['background'] = array('red' => '255', 'green' => '255', 'blue' => '255');
        $combos['colourwhiteblack']['fontcolour'] = array('red' => '0', 'green' => '0', 'blue' => '0');
        $combos['colourblackwhite']['background'] = array('red' => '0', 'green' => '0', 'blue' => '0');
        $combos['colourblackwhite']['fontcolour'] = array('red' => '255', 'green' => '255', 'blue' => '255');
        $combos['colourgreyblack']['background'] = array('red' => '204', 'green' => '204', 'blue' => '204');
        $combos['colourgreyblack']['fontcolour'] = array('red' => '0', 'green' => '0', 'blue' => '0');
        $combos['colourbluebrown']['background'] = array('red' => '197', 'green' => '239', 'blue' => '253');
        $combos['colourbluebrown']['fontcolour'] = array('red' => '35', 'green' => '31', 'blue' => '32');
        $combos['colourpeachbrown']['background'] = array('red' => '236', 'green' => '197', 'blue' => '168');
        $combos['colourpeachbrown']['fontcolour'] = array('red' => '55', 'green' => '47', 'blue' => '24');
        $combos['colouryellowblack']['background'] = array('red' => '254', 'green' => '252', 'blue' => '215');
        $combos['colouryellowblack']['fontcolour'] = array('red' => '0', 'green' => '0', 'blue' => '0');
        $combos['colourpinkblack']['background'] = array('red' => '255', 'green' => '233', 'blue' => '232');
        $combos['colourpinkblack']['fontcolour'] = array('red' => '0', 'green' => '0', 'blue' => '0');
        if (isset ($combos[$combo])) {
            $this->backgroundredgreenblue = $combos["$combo"]['background'];
            $this->textredgreenblue = $combos["$combo"]['fontcolour'];
        } else {
            throw new moodle_exception('colourcombinationnotexists', 'repository_xpert_url');
        }
    }

    /**
     * Replace standard with medium otherwise return the size passed to this method unchanged
     *
     * @param string $size
     * @return string $size
     */
    private function get_image_size($size) {
        if ($size == 'standard') {
            return 'medium';
        }
        return $size;
    }

    /**
     * Create extra text image for text that will fit next to the license icon (for small images)
     * (The rest of the text will fit below the license icon on $this->textimage)
     */
    private function create_extra_text_image() {

        $colours = $this->textredgreenblue;
        $background = $this->backgroundredgreenblue;
        $width = $this->width - $this->iconwidth;

        $this->extratextimage = imagecreate($width, $this->iconheight);
        imagecolorallocate($this->extratextimage, $background['red'], $background['green'], $background['blue']);
        $fontcolour = imagecolorallocate($this->extratextimage, $colours['red'], $colours['green'], $colours['blue']);
        if ($this->size === 'small' &&
                ($this->layout === self::LAYOUT_ATTRIBUTION_BESIDE_LICENSE || $this->layout === self::LAYOUT_URL_BESIDE_LICENSE)) {
            $y = ($this->iconheight - $this->textheight) / 2 + $this->textheight - 1;
        } else {
            $y = ($this->iconheight - $this->textheight) / 2 + $this->textheight - $this->ybaseline;
        }
        $x = 2;
        $angle = 0;
        imagettftext($this->extratextimage, $this->fontsize, $angle, $x, $y, $fontcolour, $this->font, $this->texttofit);
    }

    /**
     * Create filler image below the license icon if the textimage is taller than the license icon
     */
    private function create_filler_image() {
        $fontcolour = $this->textredgreenblue;
        $background = $this->backgroundredgreenblue;
        $this->fillerimageheight = $this->textimageheight - $this->iconheight;
        $this->fillerimage = imagecreate($this->iconwidth, $this->fillerimageheight);
        imagecolorallocate($this->fillerimage, $background['red'], $background['green'], $background['blue']);
        imagecolorallocate($this->fillerimage, $fontcolour['red'], $fontcolour['green'], $fontcolour['blue']);
    }

    /**
     * Create main image which will hold the copyright text
     */
    private function create_text_image() {
        $fontcolour = $this->textredgreenblue;
        $background = $this->backgroundredgreenblue;
        $this->set_textimage_height();

        if ($this->size == 'small' && $this->layout <> self::LAYOUT_MIXED_ALIGN_URL
                && ($this->yearauthor <> '' || $this->url <> '')) {
            $this->newheight = $this->height + $this->iconheight + $this->textimageheight;
        } else {
            $this->newheight = $this->height + $this->textimageheight;
        }

        $this->masterimage = imagecreatetruecolor($this->width, $this->newheight);
        imagealphablending($this->masterimage, false);

        $this->textimage = imagecreate($this->textimagewidth, $this->textimageheight);
        imagecolorallocate($this->textimage, $background['red'], $background['green'], $background['blue']);
        $this->fontcolour = imagecolorallocate($this->textimage, $fontcolour['red'], $fontcolour['green'], $fontcolour['blue']);
    }

    /**
     * Calculate the height of the image which will contain the copyright text.
     */
    private function set_textimage_height() {

        $this->totallines = $this->textlines + $this->urllines + $this->extralines;
        $this->textimageheight = $this->totallines * ($this->textheight + ($this->ybaseline * 2));
        if ($this->textimageheight < $this->iconheight) {
            $this->textimageheight = $this->iconheight;
        }
    }

    /**
     * Create the copyright license icon and set it's height & width
     */
    private function set_licenseicon_properties() {
        $licensepath = __DIR__ . '/../pix/' . $this->size . '_' . $this->license . '.png';
        if ($this->license == 'unknown' || $this->license == 'allrightsreserved') {
            $this->licenseicon = 'none';
            $this->iconheight = 0;
            $this->iconwidth = 1;
        } else {
            $this->licenseicon = imagecreatefrompng($licensepath);
            $this->iconheight = imagesy($this->licenseicon) + 1;
            $this->iconwidth = imagesx($this->licenseicon);
        }
    }

    /**
     * Get the copyright information text (Year and author) to be displayed
     *
     * @param int $year
     * @param string $author
     * @param string $url
     * @return string $yearauthor
     */
    private function get_yearauthor($year, $author, $url) {
        // No year and no author.
        if ($year == '0' && $author == '') {
            $yearauthor = '';
            // Auther name but no year.
        } else if ($year == '0' && $author <> '') {
            $yearauthor = "© " . $author;
            // Year + Author name.
        } else if ($author <> '') {
            $yearauthor = "© " . $year . ' - ' . $author;
            // Year + URL.
        } else if ($url <> '') {
            $yearauthor = '';
            // Just year.
        } else {
            $yearauthor = "© " . $year;
        }
        return $yearauthor;
    }

    /**
     * Wrap copyright text that is too long for the width of the image.
     *
     * @param string $text
     * @param int $textperline
     * @param int $linenumber
     * @param int $textlines
     * @return void
     */
    private function wrap_image_text($text, $textperline, $linenumber, $textlines) {
        // For small images with attribution text below the license icon on one line, use a different y coordinate.
        if ($this->size === 'small' && $this->textfits &&
                ($this->layout === self::LAYOUT_ATTRIBUTION_BELOW_LICENSE || $this->layout === self::LAYOUT_URL_BESIDE_LICENSE)
                && $this->totallines == 1) {
            $y = ($this->iconheight - $this->textheight) / 2 + $this->textheight;
            imagettftext($this->textimage, $this->fontsize, 0, 2, $y, $this->fontcolour, $this->font, $text);
            return;
        }
        // Check for specific layouts / size to determine the layout of copyright information.
        if (($this->layout === self::LAYOUT_DEFAULT
                || ($this->layout === self::LAYOUT_URL_BESIDE_LICENSE && $this->size === 'small'))
                && $this->totallines == 2) {
            // Center the text within the space provided based on the textimageheight, totallines & text height.
            $linebottom = floor($this->textimageheight / $this->totallines) - floor($this->textheight / 2) + 3;
        } else {
            $linebottom = floor((($this->textimageheight / $this->totallines) - $this->textheight) / 2 +
                    $this->textheight + $this->ybaseline);
        }
        $linecount = 0;
        $textcursor = 0;
        while ($linecount < $textlines) {
            $linecount++;
            $linestring = substr($text, $textcursor, $textperline);
            $height = ($linenumber * $linebottom);
            imagettftext($this->textimage, $this->fontsize, 0, 2, $height - $this->ybaseline, $this->fontcolour,
                    $this->font, $linestring);
            $linenumber++;
            $textcursor += $textperline;
        }
    }

    /**
     * Get characters count and characters per line
     *
     * @param string $text
     * @param int $textwidth
     * @return \stdclass
     */
    public function get_characters_info($text, $textwidth) {
        $charactercount = strlen($text);
        // Set the box width ratio.  The higher the ratio the less characters per line.
        // Increase this ratio by 5% to prevent characters overflowing off the image.
        // We don't know the exact width at the perfect cut-off point so use 5% playroom instead.
        $boxwidthratio = $textwidth / $this->textimagewidth * 1.05;
        $charactersperline = floor($charactercount / $boxwidthratio);
        $characters = new stdclass();
        $characters->perline = $charactersperline;
        $characters->count = $charactercount;
        return $characters;
    }

    /**
     * Add license image, copyright year, author and URL to the original image
     */
    public function add_attribution() {

        if ($this->debugging) {
            $this->image = $this->debugimage;
        }
        // Copy the creative commons icon on to the blank master image.
        if ($this->licenseicon <> 'none') {
            imagecopy($this->masterimage, $this->licenseicon, 0, $this->height + 1, 0, 0, $this->iconwidth, $this->iconheight);
        }
        // Add extra image next to the license icon if there is text that can fit in the empty space.
        if ($this->size == 'small' && $this->textfits && !$this->aligntext) {
            imagecopy($this->masterimage, $this->extratextimage, $this->iconwidth, $this->height + 1, 0, 0,
                    $this->boxwidth, $this->iconheight);
        }
        // Copy the copyright text as an image on to the blank master image.
        if ($this->textimage <> 'none') {
            imagecopy($this->masterimage, $this->textimage, $this->xposition, $this->yposition, 0, 0,
                    $this->textimagewidth, $this->textimageheight);
        }
        // Add filler image for medium / large images were the text has overflowed into extra lines.
        if ($this->fillerimage) {
            imagecopy($this->masterimage, $this->fillerimage, 0, $this->height + $this->iconheight + 1, 0, 0,
                    $this->iconwidth, $this->fillerimageheight);
        }
        // Copy the original images on to the blank master image.
        if ($this->licenseicon == 'none' && $this->textimage == 'none') {
            return;
        } else {
            imagecopy($this->masterimage, $this->image, 0, 0, 0, 0, $this->width, $this->height);
            $this->image = $this->masterimage;
        }
    }

    /**
     * Get the height and width of the text based on the font and font size
     *
     * @param string $text
     * @return string
     */
    public function get_text_dimensions($text) {

        $dimensions = array();
        $textbox = imagettfbbox($this->fontsize, 0, $this->font, $text);
        $dimensions['width'] = abs($textbox[4]) - abs($textbox[0]); // Distance from left to right.
        $dimensions['height'] = abs($textbox[5]) - abs($textbox[1]); // Distance from top to bottom.
        if ($this->size == 'large') {
            $dimensions['ybaseline'] = 5;
        } else {
            $dimensions['ybaseline'] = 3;
        }
        return $dimensions;
    }

    /**
     * Check whether the text fits within the image
     *
     * @param string $textwidth
     * @param string $imagewidth
     * @return boolean
     */
    public static function text_fits_image($textwidth, $imagewidth) {
        return ($textwidth < $imagewidth);
    }

    /**
     * Calculate how many extra lines are needed to print the text on the image (if any)
     *
     * @param string $textwidth
     * @param string $imagewidth
     * @return string
     */
    public static function add_lines($textwidth, $imagewidth) {
        $extralines = 0;
        if (!$imagewidth || $imagewidth < 0) {
            throw new moodle_exception('incorrectimagewidth');
        }
        if (!self::text_fits_image($textwidth, $imagewidth)) {
            $extralines = ceil($textwidth / $imagewidth) - 1;
        }
        return $extralines;
    }

    /**
     * Store specific info for each image size (font size, lines, strings)
     */
    public function set_text_info() {

        // Height and width of the copyright text.
        $urldimensions = $this->get_text_dimensions($this->url);
        $textdimensions = $this->get_text_dimensions($this->yearauthor);
        $hyphen = $this->get_text_dimensions(' - ');
        $this->urlwidth = $urldimensions['width'];
        $urlheight = $urldimensions['height'];
        $this->textwidth = $textdimensions['width'];
        $this->textheight = $textdimensions['height'];
        $this->ybaseline = $textdimensions['ybaseline'];
        // Use the heighest of the text / url height.
        if ($urlheight > $this->textheight) {
            $this->textheight = $urlheight;
        }
        $this->boxwidth = $this->width - $this->iconwidth;
        $this->layout = self::LAYOUT_DEFAULT;
        // For small images, check what can fit in where.
        if ($this->size == 'small') {
            if ($this->textwidth + $hyphen['width'] + $this->urlwidth < $this->boxwidth) {
                // Year + Author + URL can all fit next to the license icon.
                $this->set_layout_attribution_beside_license();
            } else if ($this->textwidth + $hyphen['width'] + $this->urlwidth < $this->width) {
                // Year + Author + URL can all fit below the license icon.
                $this->set_layout_attribution_below_license();
            } else if ($this->textwidth < $this->boxwidth) {
                // The Year + Author fit next to the license icon.
                if ($this->urlwidth < $this->boxwidth) {
                    // The URL fits below the Year + Author text aligned at the same x position.
                    $this->set_layout_mixed_align_url();
                } else {
                    // The URL is too long to align below the Year + Author so needs to starts below the license icon.
                    $this->set_layout_mixed_overflow_url();
                }
            } else if ($this->urlwidth < $this->boxwidth) {
                // The URL can fit next to the license icon.
                $this->set_layout_url_beside_license();
            } else { // This could only happen for strange images & text.
                // Nether the URL nor the year + author text can fit next to the license icon without overflowing to a new line.
                $this->set_layout_overflow_all();
            }
        }
        // Calculate how many extra lines are needed for wide text / url.
        $this->urllines += self::add_lines($this->urlwidth, $this->boxwidth);
        // Set URL lines to zero if URL is set to be empty.
        if ($this->url == '') {
            $this->urllines = 0;
        }
        $this->textlines += self::add_lines($this->textwidth, $this->boxwidth);
        if ($this->yearauthor == '') {
            $this->textlines = 0;
        }
        $this->textimagewidth = $this->width;
        if ($this->size !== 'small') {
            // Handle copyright text for medium and large images.
            $this->textimagewidth = $this->width - $this->iconwidth;
        }
        // Get characters count and characters per line for the Year + Author section of the copyright text.
        $this->textchars = $this->get_characters_info($this->yearauthor, $this->textwidth);
        // Get characters count and characters per line for the URL section of the copyright text.
        $this->urlchars = $this->get_characters_info($this->url, $this->urlwidth);
    }

    /**
     * Set the layout properties
     * Year + Author + URL can all fit next to the license icon.
     */
    private function set_layout_attribution_beside_license() {
        $this->texttofit = $this->yearauthor . ' - ' . $this->url;
        // We have moved the URL and Year + Author text into a separate image ($this->extratextimage).
        // $this->extratextimage will always use $this->iconwidth and $this->height + 1 as the x and y values.
        $this->url = '';
        $this->yearauthor = '';
        $this->textfits = true;
        $this->layout = self::LAYOUT_ATTRIBUTION_BESIDE_LICENSE;
    }

    /**
     * Set the layout properties
     * Year + Author + URL can all fit below the license icon.
     */
    private function set_layout_attribution_below_license() {
        $this->yearauthor = $this->yearauthor . ' - ' . $this->url;
        $this->layout = self::LAYOUT_ATTRIBUTION_BELOW_LICENSE;
        $this->url = ''; // We have combined the year + author + url in $this->yearauthor above.
        $this->boxwidth = $this->width;
        $this->textfits = true; // This allows this->extratextimage to be printed next to the license icon.
    }

    /**
     * Set the layout properties
     * The Year + Author fit next to the license icon.
     * The URL fits below the Year + Author text aligned at the same x position.
     */
    private function set_layout_mixed_align_url() {
        $this->layout = self::LAYOUT_MIXED_ALIGN_URL;
        // Since the URL can fit below the Year + Author (aligned) we can use $this->textimage.
        // Since we are using $this->textimage we need to set the x and y position where it should be placed.
        $this->xposition = $this->iconwidth;
        $this->yposition = $this->height + 1;
    }

    /**
     * Set the layout properties
     * The Year + Author fit next to the license icon.
     * The URL is too long to align below the Year + Author so needs to starts below the license icon.
     */
    private function set_layout_mixed_overflow_url() {
        $this->layout = self::LAYOUT_MIXED_OVERFLOW_URL;
        // The URL does not fit aligned below the Year + Author text (next to the license icon).
        // The URL will need to go below the license icon on a separate image.  If it's a long URL it might overflow
        // to a new line, hence we need to set $this->boxwidth to the full width of the master image.
        $this->boxwidth = $this->width;
        $this->texttofit = $this->yearauthor;
        // We have moved the Year + Author text into a separate image ($this->extratextimage).
        // $this->extratextimage will always use $this->iconwidth and $this->height + 1 as the x and y values.
        $this->yearauthor = ''; // $this->texttofit now contains the Year + Author.
        $this->textfits = true;  // This will allow $this->extratextimage to be printed next to the icon.
    }

    /**
     * Set the layout properties
     * The URL can fit next to the license icon.
     */
    private function set_layout_url_beside_license() {
        $this->layout = self::LAYOUT_URL_BESIDE_LICENSE;
        $this->texttofit = $this->url;
        $this->url = ''; // We have move the URL text into $this->texttofit.
        $this->textfits = true; // This allows this->extratextimage (with URL text) to be printed next to the license icon.
        $this->boxwidth = $this->width; // This is for the Year + Author text which will fit below the license icon.
    }

    /**
     * Set the layout properties
     * Nether the URL nor the year + author text can fit next to the license icon without overflowing to a new line.
     */
    private function set_layout_overflow_all() {
        // Neither the Year + Author or URL text can fit next to the license icon without overflowing.
        $this->layout = self::LAYOUT_OVERFLOW_ALL;
        $this->textfits = true; // This allows this->extratextimage to be printed next to the license icon as a filler.
        $this->boxwidth = $this->width;  // All text will fit below the license icon.
    }

    /**
     * Set the font size based on the image size selected
     */
    public function set_font_size() {
        switch ($this->size) {
            case 'large':
                $this->fontsize = 12;
                break;
            case 'small':
                $this->fontsize = 7;
                break;
            default:
                $this->fontsize = 8;
        }
    }

    /**
     * set the x & yposition for placing the image containing the copyright text
     */
    private function set_xy_position() {
        if ($this->licenseicon == 'none') {
            $this->xposition = $this->iconwidth;
            $this->yposition = $this->height + 1;
        } else {
            if ($this->size == 'small') {
                // For small images we will print the copyright text below the license icon.
                $this->xposition = 0;
                $this->yposition = $this->height + $this->iconheight + 1;
                $this->aligntext = false; // We assume for now that the text will not be aligned.
            } else {
                // For medium/standard and large images we will print the copyright text next to the license icon.
                $this->xposition = $this->iconwidth;
                $this->yposition = $this->height + 1;
            }
        }
    }

    /**
     * Create an image to hold the debug info and add text to it.
     */
    private function add_debug_info() {
        $fontsize = 6;
        // Create the image which will contain the debug text.
        $this->debugimage = imagecreate($this->width, $this->height);
        imagecolorallocate($this->debugimage, 0, 0, 128);
        $white = imagecolorallocate($this->debugimage, 255, 255, 255);
        $line = $this->textheight + 1;
        $wide = 14 * $line;
        $license = end(explode('/', $this->license));

        imagettftext($this->debugimage, $fontsize, 0, 2, $line, $white, $this->font,
                'Fontsize: ' . $this->fontsize . ', Y-baseline: ' . $this->ybaseline);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line, $white, $this->font,
                'License: ' . $license);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 2, $white, $this->font,
                'Layout option: ' . $this->layout);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 3, $white, $this->font,
                'Year + Author: ' .$this->yearauthor);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 4, $white, $this->font,
                'URL: ' . $this->url);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 5, $white, $this->font,
                'Extra text: ' . $this->texttofit);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 6, $white, $this->font,
                'Original Image height: ' . $this->height);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 6, $white, $this->font,
                'Width: ' . $this->width);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 7, $white, $this->font,
                'Master Image height: ' . $this->newheight);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 7, $white, $this->font,
                'Text height: ' . $this->textheight . ', Width: ' . $this->textwidth);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 8, $white, $this->font,
                'Icon Height: ' . $this->iconheight);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 8, $white, $this->font,
                'Icon Width: ' . $this->iconwidth);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 9, $white, $this->font,
                'Text image height: ' . $this->textimageheight);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 9, $white, $this->font,
                'Text image width: ' . $this->textimagewidth);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 10, $white, $this->font,
                'Text chars count: ' . $this->textchars->count);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 10, $white, $this->font,
                'Text chars per line: ' . $this->textchars->perline);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 11, $white, $this->font,
                'URL chars count: ' . $this->urlchars->count);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 11, $white, $this->font,
                'URL chars per line: ' . $this->urlchars->perline);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 12, $white, $this->font,
                'X postion of text image: ' . $this->xposition);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 12, $white, $this->font,
                'Y postion: ' . $this->yposition);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 13, $white, $this->font,
                'Text lines: ' . $this->textlines);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 13, $white, $this->font,
                'Extra lines: ' . $this->extralines);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 14, $white, $this->font,
                'URL lines: ' . $this->urllines);
        imagettftext($this->debugimage, $fontsize, 0, $wide / 2, $line * 14, $white, $this->font,
                'Width: ' . $this->urlwidth);
        imagettftext($this->debugimage, $fontsize, 0, $wide, $line * 14, $white, $this->font,
                'Total lines: ' . $this->totallines);
        imagettftext($this->debugimage, $fontsize, 0, 2, $line * 15, $white, $this->font,
                'Image path: ' . $this->imagepath);
    }

    /**
     * Determine whether the debug setting has been activated and whether the user is a site admin
     *
     * @return boolean
     */
    private function check_debugging_status() {
        $debug = trim(get_config('xpert_url', 'debug'));
        $context = context_system::instance();
        $siteadmin = has_capability('repository/xpert_url:debug', $context);
        if ($debug && $siteadmin) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the max height and width for different image sizes
     *
     * @return array $size
     */
    public static function get_sizes() {

        $size = array();
        $size['medium'] = array('width' => 500, 'height' => 375);
        $size['standard'] = array('width' => 500, 'height' => 375);
        $size['large'] = array('width' => 1024, 'height' => 768);
        $size['small'] = array('width' => 240, 'height' => 180);
        return $size;
    }

    /**
     * Get the content of the image to be passed back to the create_file_from_string method of the file_storage class
     *
     * @return string $content
     * @throws file_exception
     */
    public function get_image() {

        ob_start();
        switch ($this->info['mime']) {
            case 'image/gif':
                imagegif($this->image);
                break;
            case 'image/jpeg':
                imagejpeg($this->image);
                break;
            case 'image/png':
                imagepng($this->image);
                break;
            default:
                throw new file_exception('storedfileproblem', 'Unsupported mime type');
        }
        $content = ob_get_contents();
        ob_end_clean();
        imagedestroy($this->image);
        if (!$content) {
            throw new file_exception('storedfileproblem', 'Can not convert image');
        }
        return $content;
    }

    /**
     * Get the text for the URL to be displayed as part of the copyright text
     *
     * @param string $url
     * @param int $year
     * @return string
     */
    private function get_url($url, $year) {

        if ($url <> '' && $this->yearauthor == '') {
            if ($year <> '') {
                $url = '© ' . $year . ' ' . $url;
            } else {
                $url = '© ' . $url;
            }
        }
        return $url;
    }


    /**
     * Resize the uploaded image based on the size selected and retain height / width ratio
     *
     * @return resource $resizedimage
     */
    private function resize_image() {
        $sizes = self::get_sizes();
        $resizeheight = $sizes[$this->size]['height'];
        $resizewidth = $sizes[$this->size]['width'];
        $this->info = getimagesize($this->imagepath);

        switch ($this->info['mime']) {
            case 'image/jpeg':
                $image = imagecreatefromjpeg($this->imagepath);
                break;
            case 'image/png':
                $image = imagecreatefrompng($this->imagepath);
                break;
            case 'image/gif':
                if (!function_exists('imagecreatefromgif')) {
                    throw new moodle_exception('gdgifnotexist');
                }
                $image = imagecreatefromgif($this->imagepath);
                break;
            default:
                break;
        }

        $originalwidth = $this->info[0];
        $originalheight = $this->info[1];
        if ($originalheight > $originalwidth) {
            $resizeratio = $resizeheight / $originalheight;
        } else {
            $resizeratio = $resizewidth / $originalwidth;
        }
        $this->height = $originalheight * $resizeratio;
        $this->width = $originalwidth * $resizeratio;

        $resizedimage = imagecreatetruecolor($this->width, $this->height);
        imagealphablending($resizedimage, false);
        imagecopyresampled($resizedimage, $image, 0, 0, 0, 0, $this->width, $this->height, $originalwidth, $originalheight);

        return $resizedimage;
    }

    /**
     * Save the image
     * @param string $imagepath
     * @return boolean|\repository_xpert_url_image
     */
    public function saveas($imagepath) {

        switch ($this->info['mime']) {
            case 'image/jpeg':
                return imagejpeg($this->image, $imagepath);
            case 'image/png':
                return imagepng($this->image, $imagepath);
            case 'image/gif':
                return imagegif($this->image, $imagepath);
            default:
                break;
        }
        $this->destroy();
        return $this;
    }

    /**
     * Destroy the image
     * @return boolean
     */
    public function destroy() {
        imagedestroy($this->image);
        imagedestroy($this->backup);
        return true;
    }
}
